﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class Oda08Reto2 : Challenge
    {
        #region Enumeration

        public new enum Action
        {
            ShowEnemy, ShowProfessor
        }

        #endregion

        #region Attributes

        private Animator animPuerta, animZoom;
        private bool esRetoActivo, esZoomInTerminado;
        private Button btnPuertar2, btnPocion1r2, btnPocion2r2, btnPocion3r2, btnPocion4r2, btnCapaToreror2, btnRioQueCantar2, btnPezuiaDeToror2, btnEsenciaDeClavelr2, btnLaberintor2, btnHadaNegrar2, btnCabelloDeSansonr2, btnLantoDeHerculesr2,
            btnSalivaPieGrander2, btnSalivaCaracolr2, btnPozoDeLaPerdicionr2, btnCuernoUnicornior2, btnTierraDeCementerior2, btnCapaMerilinr2, btnCaparazonMolidor2,btnCapulloMolidor2, btnAceptarLiquidosr2,
            btnAceptarSolidosr2, btnBorrarSolidosr2, btnTeclado0r2, btnTeclado1r2, btnTeclado2r2, btnTeclado3r2, btnTeclado4r2, btnTeclado5r2, btnTeclado6r2, btnTeclado7r2, btnTeclado8r2, btnTeclado9r2;
        private UnityEngine.GameObject goInicio, goProfesorr2, goGloboProfesorr2, goLaboratorior2, goEnemigor2, goPocionesr2, goRecetar2, goMezclar2, goZonaPreparacionr2, goIngredientesr2, goZonaBloqueor2, goMedidorLiquidosr2, goMedidorSolidosr2;
        private Image imgInterfazr2, imgRecetar2, imgAreaInteractiva;
        private int numPocionActual, numIngredientesServidos;
        private List<string> lsIngredientesr2;
        private List<ERecipe> loRecetasr2;
        private Slider sldMedidorLiquidosr2;
        private string ingredienteActual, proporcionSolido;
        private Text txtRecetar2, txtProporcionSolidor2;
        private Button btnMezclarr2;

        #endregion

        #region Constructor

        public Oda08Reto2() : base("Reto2")
		{
			
		}

		#endregion

		#region Events

		#region Overridden

		// Use this for initialization
		protected override void Start()
        {
            #region Object instances

            lsIngredientesr2 = new List<string> {
"CapaToreror2", "RioQueCantar2",
"PezuiaDeToror2", "EsenciaDeClavelr2",
"Laberintor2", "HadaNegrar2",
"CabelloDeSansonr2", "LantoDeHerculesr2",
"SalivaPieGrander2", "SalivaCaracolr2",
"PozoDeLaPerdicionr2", "CuernoUnicornior2","CaparazonMolidor2","CapulloMolidor2",
"TierraDeCementerior2", "CapaMerilinr2"
            };

            goInicio = Canvas.transform.Find("Inicio").gameObject;
            goLaboratorior2 = Canvas.transform.Find("Laboratorior2").gameObject;

            btnPuertar2 = goInicio.transform.Find("btnPuertar2").GetComponent<Button>();
            goProfesorr2 = goInicio.transform.Find("Profesorr2").gameObject;

            goEnemigor2 = goLaboratorior2.transform.Find("Enemigor2").gameObject;

            animPuerta = btnPuertar2.GetComponent<Animator>();
            animPuerta.speed = 0f;

            goGloboProfesorr2 = goProfesorr2.transform.Find("GloboProfesorr2").gameObject;

            imgInterfazr2 = goEnemigor2.transform.Find("imgInterfazr2").GetComponent<Image>();
            goZonaPreparacionr2 = goEnemigor2.transform.Find("ZonaPreparacionr2").gameObject;

            goPocionesr2 = imgInterfazr2.transform.Find("Pocionesr2").gameObject;
            goRecetar2 = imgInterfazr2.transform.Find("Recetar2").gameObject;
            goMezclar2 = imgInterfazr2.transform.Find("Mezclar2").gameObject;

            btnPocion1r2 = goPocionesr2.transform.Find("btnPocion1r2").GetComponent<Button>();
            btnPocion2r2 = goPocionesr2.transform.Find("btnPocion2r2").GetComponent<Button>();
            btnPocion3r2 = goPocionesr2.transform.Find("btnPocion3r2").GetComponent<Button>();
            btnPocion4r2 = goPocionesr2.transform.Find("btnPocion4r2").GetComponent<Button>();

           btnMezclarr2 = goMezclar2.transform.Find("btnMezclarr2").GetComponent<Button>();

            imgRecetar2 = goRecetar2.transform.Find("imgRecetar2").GetComponent<Image>();

            txtRecetar2 = imgRecetar2.transform.Find("txtRecetar2").GetComponent<Text>();

            goIngredientesr2 = goZonaPreparacionr2.transform.Find("Ingredientesr2").gameObject;
            goZonaBloqueor2 = goZonaPreparacionr2.transform.Find("ZonaBloqueor2").gameObject;

            btnCapaToreror2 = goIngredientesr2.transform.Find("btnCapaToreror2").GetComponent<Button>();
            btnRioQueCantar2 = goIngredientesr2.transform.Find("btnRioQueCantar2").GetComponent<Button>();
            btnPezuiaDeToror2 = goIngredientesr2.transform.Find("btnPezuiaDeToror2").GetComponent<Button>();
            btnEsenciaDeClavelr2 = goIngredientesr2.transform.Find("btnEsenciaDeClavelr2").GetComponent<Button>();
            btnLaberintor2 = goIngredientesr2.transform.Find("btnLaberintor2").GetComponent<Button>();
            btnHadaNegrar2 = goIngredientesr2.transform.Find("btnHadaNegrar2").GetComponent<Button>();
            btnCabelloDeSansonr2 = goIngredientesr2.transform.Find("btnCabelloDeSansonr2").GetComponent<Button>();
            btnLantoDeHerculesr2 = goIngredientesr2.transform.Find("btnLantoDeHerculesr2").GetComponent<Button>();
            btnSalivaPieGrander2 = goIngredientesr2.transform.Find("btnSalivaPieGrander2").GetComponent<Button>();
            btnSalivaCaracolr2 = goIngredientesr2.transform.Find("btnSalivaCaracolr2").GetComponent<Button>();
            btnPozoDeLaPerdicionr2 = goIngredientesr2.transform.Find("btnPozoDeLaPerdicionr2").GetComponent<Button>();
            btnCuernoUnicornior2 = goIngredientesr2.transform.Find("btnCuernoUnicornior2").GetComponent<Button>();
            btnTierraDeCementerior2 = goIngredientesr2.transform.Find("btnTierraDeCementerior2").GetComponent<Button>();
            btnCapaMerilinr2 = goIngredientesr2.transform.Find("btnCapaMerilinr2").GetComponent<Button>();

            goIngredientesr2.transform.Find("imgCapaToreror2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgCapaToreror2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgRioQueCantar2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgPezuiaDeToror2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgEsenciaDeClavelr2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgLaberintor2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgHadaNegrar2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgCabelloDeSansonr2").GetComponent<Image>().enabled = false;

            goIngredientesr2.transform.Find("imgLantoDeHerculesr2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgSalivaPieGrander2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgSalivaCaracolr2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgPozoDeLaPerdicionr2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgCuernoUnicornior2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgCaparazonMolidor2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgCapulloMolidor2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgTierraDeCementerior2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgCapaMerilinr2").GetComponent<Image>().enabled = false;
           


            foreach (string ingrediente in lsIngredientesr2)
            {
                goIngredientesr2.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = false;
                goIngredientesr2.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>().speed = 0f;
                goIngredientesr2.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSpriter2").GetComponent<Image>().enabled = false;
                goIngredientesr2.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillor2").GetComponent<Image>().enabled = false;
            }

            goMedidorLiquidosr2 = goZonaPreparacionr2.transform.Find("MedidorLiquidosr2").gameObject;
            goMedidorSolidosr2 = goZonaPreparacionr2.transform.Find("MedidorSolidosr2").gameObject;

            btnAceptarLiquidosr2 = goMedidorLiquidosr2.transform.Find("btnAceptarr2").GetComponent<Button>();
            sldMedidorLiquidosr2 = goMedidorLiquidosr2.transform.Find("imgProbetar2").GetComponent<Image>().transform.Find("sldMedidorr2").GetComponent<Slider>();

            imgAreaInteractiva = sldMedidorLiquidosr2.GetComponentsInChildren<Image>().FirstOrDefault(item => item.name == "imgAreaInteractiva");

            txtProporcionSolidor2 = goMedidorSolidosr2.transform.Find("imgBascular2").GetComponent<Image>().transform.Find("txtBascular2").GetComponent<Text>();
            btnAceptarSolidosr2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnAceptarr2").GetComponent<Button>();
            btnBorrarSolidosr2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnBorrarr2").GetComponent<Button>();
            btnTeclado0r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla0r2").GetComponent<Button>();
            btnTeclado1r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla1r2").GetComponent<Button>();
            btnTeclado2r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla2r2").GetComponent<Button>();
            btnTeclado3r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla3r2").GetComponent<Button>();
            btnTeclado4r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla4r2").GetComponent<Button>();
            btnTeclado5r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla5r2").GetComponent<Button>();
            btnTeclado6r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla6r2").GetComponent<Button>();
            btnTeclado7r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla7r2").GetComponent<Button>();
            btnTeclado8r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla8r2").GetComponent<Button>();
            btnTeclado9r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla9r2").GetComponent<Button>();

            InicializarPociones();

            #endregion

            #region Event listeners

            btnPuertar2.onClick.AddListener(MostrarLaboratorio);
            btnPocion1r2.onClick.AddListener(delegate
            {
                MostrarReceta(1);
                HabilitarZonaPreparacion();
            });
            btnPocion2r2.onClick.AddListener(delegate
            {
                MostrarReceta(2);
                HabilitarZonaPreparacion();
            });
            btnPocion3r2.onClick.AddListener(delegate
            {
                MostrarReceta(3);
                HabilitarZonaPreparacion();
            });
            btnPocion4r2.onClick.AddListener(delegate
            {
                MostrarReceta(4);
                HabilitarZonaPreparacion();
            });
            btnMezclarr2.onClick.AddListener(MezclarIngredientes);


            btnCapaToreror2.onClick.AddListener(delegate
            {
                ZoomIn("CapaToreror2 ");
            });
            btnRioQueCantar2.onClick.AddListener(delegate
            {
                ZoomIn("RioQueCantar2 ");
            });
            btnPezuiaDeToror2.onClick.AddListener(delegate
            {
                ZoomIn("PezuiaDeToror2 ");
            });
            btnEsenciaDeClavelr2.onClick.AddListener(delegate
            {
                ZoomIn("EsenciaDeClavelr2 ");
            });
            btnLaberintor2.onClick.AddListener(delegate
            {
                ZoomIn("Laberintor2");
            });
            btnHadaNegrar2.onClick.AddListener(delegate
            {
                ZoomIn("btnHadaNegrar2");
            });
            btnCabelloDeSansonr2.onClick.AddListener(delegate
            {
                ZoomIn("btnCabelloDeSansonr2");
            });
            btnLantoDeHerculesr2.onClick.AddListener(delegate
            {
                ZoomIn("btnLantoDeHerculesr2");
            });
            btnSalivaPieGrander2.onClick.AddListener(delegate
            {
                ZoomIn("btnSalivaPieGrander2");
            });
            btnSalivaCaracolr2.onClick.AddListener(delegate
            {
                ZoomIn("btnSalivaCaracolr2");
            });
            btnPozoDeLaPerdicionr2.onClick.AddListener(delegate
            {
                ZoomIn("btnPozoDeLaPerdicionr2");
            });
            btnCuernoUnicornior2.onClick.AddListener(delegate
            {
                ZoomIn("btnCuernoUnicornior2");
            });
            btnTierraDeCementerior2.onClick.AddListener(delegate
            {
                ZoomIn("btnTierraDeCementerior2");
            });
            btnCapaMerilinr2.onClick.AddListener(delegate
            {
                ZoomIn("btnCapaMerilinr2");
            });
            btnAceptarLiquidosr2.onClick.AddListener(GuardarProporcion);
            btnAceptarSolidosr2.onClick.AddListener(GuardarProporcion);
            btnBorrarSolidosr2.onClick.AddListener(BorrarProporcion);
            btnTeclado0r2.onClick.AddListener(delegate
            {
                SumarProporcion(0);
            });
            btnTeclado1r2.onClick.AddListener(delegate
            {
                SumarProporcion(1);
            });
            btnTeclado2r2.onClick.AddListener(delegate
            {
                SumarProporcion(2);
            });
            btnTeclado3r2.onClick.AddListener(delegate
            {
                SumarProporcion(3);
            });
            btnTeclado4r2.onClick.AddListener(delegate
            {
                SumarProporcion(4);
            });
            btnTeclado5r2.onClick.AddListener(delegate
            {
                SumarProporcion(5);
            });
            btnTeclado6r2.onClick.AddListener(delegate
            {
                SumarProporcion(6);
            });
            btnTeclado7r2.onClick.AddListener(delegate
            {
                SumarProporcion(7);
            });
            btnTeclado8r2.onClick.AddListener(delegate
            {
                SumarProporcion(8);
            });
            btnTeclado9r2.onClick.AddListener(delegate
            {
                SumarProporcion(9);
            });

            #endregion
        }

        // Update is called once per frame
        protected override void Update()
        {
            #region Initial state

            if (Instance.activeInHierarchy)
            {
                if (!esRetoActivo)
                {
                    GenericUi.ShowInfoPopUp(
                        " reto 2  -- ¡La escuela de magia Kadabra, se alegra de tenerte entre sus estudiantes! Deberás esforzarte porque el profesor Galaguer es uno de los más exigentes",
                        new ESequence
                        {
                            Action = (Challenge.Action)Action.ShowProfessor,
                            ChallengeNum = 2,
                            Event = Event.ClosePopUp
                        }, 0.0f
                    );
                    esRetoActivo = true;
                }
                else
                {
                    if (!string.IsNullOrEmpty(ingredienteActual) && !esZoomInTerminado &&
                        animZoom.GetCurrentAnimatorStateInfo(0).IsName(ingredienteActual) &&
                        animZoom.GetCurrentAnimatorStateInfo(0).normalizedTime % animZoom.GetCurrentAnimatorStateInfo(0).length > 1.0f)
                        ZoomOut(ingredienteActual);
                }
            }
            else
                esRetoActivo = false;

            #endregion

            AnimateText();
        }

        #endregion

        #region Private

        private void ClosePopUp(Action action)
        {
            switch (action)
            {
                #region Cases

                case Action.ShowEnemy:
                    goEnemigor2.SetActive(true);
                    break;
                case Action.ShowProfessor:
                    goProfesorr2.SetActive(true);
                    animPuerta.Play("Puerta");
                    animPuerta.speed = 1f;
                    Invoke("MostrarGloboProfesorr2", 0.45f);
                    break;

                    #endregion
            }

            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
        }

        #endregion

        #endregion

        #region Methods

        #region Private

        private void BorrarProporcion()
        {
            proporcionSolido = string.Empty;
            txtProporcionSolidor2.text = "0";
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
        }

        private void ContabilizarIngredienteServido()
        {
            goIngredientesr2.transform.Find("btn" + ingredienteActual).GetComponent<Button>().transform.Find("imgSpriter2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("btn" + ingredienteActual).GetComponent<Button>().transform.Find("imgBrillor2").GetComponent<Image>().enabled = false;
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);

            if (++numIngredientesServidos == loRecetasr2[numPocionActual - 1].Mix.Count)
                goMezclar2.SetActive(true);
        }

        private void GuardarProporcion()
        {
            if (loRecetasr2[numPocionActual - 1].Mix[ingredienteActual].Type == EIngredient.TypeIngredient.Liquid)
            {
                #region Guardar proporción de ingrediente líquido

                if ((int)sldMedidorLiquidosr2.value > 0)
                {
                    loRecetasr2[numPocionActual - 1].Mix[ingredienteActual].Proportion = Mathf.RoundToInt(sldMedidorLiquidosr2.value);
                    sldMedidorLiquidosr2.value = 0;
                    ContabilizarIngredienteServido();
                }

                goMedidorLiquidosr2.SetActive(false);

                #endregion
            }
            else
            {
                #region Guardar proporción de ingrediente sólido

                if (!string.IsNullOrEmpty(proporcionSolido))
                {
                    int proporcion = int.Parse(proporcionSolido);

                    if (proporcion != 0)
                    {
                        loRecetasr2[numPocionActual - 1].Mix[ingredienteActual].Proportion = proporcion;
                        ContabilizarIngredienteServido();
                    }

                    proporcionSolido = string.Empty;
                    txtProporcionSolidor2.text = "0";
                }

                goMedidorSolidosr2.SetActive(false);

                #endregion
            }
        }

        private void HabilitarZonaPreparacion()
        {
            goZonaBloqueor2.SetActive(false);
            GenericUi.Resume();
        }

        private void InicializarPociones()
        {
            //ReSharper disable once UseObjectOrCollectionInitializer
            loRecetasr2 = new List<ERecipe>();

            #region Initializing potion mix

            loRecetasr2.Add(new ERecipe
            {
                Dosage = 2,
                Mix = new Dictionary<string, EIngredient>
                {
                    {"CapaToreror2", new EIngredient {Proportion = 0, Quantity = 400, Type = EIngredient.TypeIngredient.Solid}},
                    {"RioQueCantar2", new EIngredient {Color = new Color32(0x99, 0xE5, 0xA3, 0xFF), Proportion = 0, Quantity = 40, Type = EIngredient.TypeIngredient.Liquid}},
                    {"PezuiaDeToror2", new EIngredient {Proportion = 0, Quantity = 28, Type = EIngredient.TypeIngredient.Solid}},
                    {"EsenciaDeClavelr2", new EIngredient {Color = new Color32(0x99, 0xE5, 0xA3, 0xFF), Proportion = 0, Quantity = 80, Type = EIngredient.TypeIngredient.Liquid}},
                    {"Laberintor2", new EIngredient {Proportion = 0, Quantity = 100, Type = EIngredient.TypeIngredient.Solid}}
                }

            });
            loRecetasr2.Add(new ERecipe
            {
                Dosage = 5,
                Mix = new Dictionary<string, EIngredient>
                {
                    {"HadaNegrar2", new EIngredient {Proportion = 0, Quantity = 250, Type = EIngredient.TypeIngredient.Solid}},
                    {"CabelloDeSansonr2", new EIngredient {Proportion = 0, Quantity = 100, Type = EIngredient.TypeIngredient.Solid}},
                    {"LlantoDeHerculesr2", new EIngredient {Color = new Color32(0x04, 0xF0, 0xFF, 0xFF), Proportion = 0, Quantity = 95, Type = EIngredient.TypeIngredient.Liquid}},
                    {"SalivaPieGrander2", new EIngredient {Color = new Color32(0x00, 0xED, 0x00, 0xFF), Proportion = 0, Quantity = 130, Type = EIngredient.TypeIngredient.Liquid}}
                }

            });
            loRecetasr2.Add(new ERecipe
            {
                Dosage = 3,
                Mix = new Dictionary<string, EIngredient>
                {
                    {"btnSalivaCaracolr2", new EIngredient {Color = new Color32 (0x16, 0xD2, 0xD9, 0xFF), Proportion = 0, Quantity = 45, Type = EIngredient.TypeIngredient.Liquid}},
                    {"btnLaberintor2", new EIngredient {Proportion = 0, Quantity = 300, Type = EIngredient.TypeIngredient.Solid}},
                    {"btnPozoDeLaPerdicionr2", new EIngredient {Color = new Color32 (0x16, 0xD2, 0xD9, 0xFF), Proportion = 0, Quantity = 120, Type = EIngredient.TypeIngredient.Liquid}},
                    {"btnCuernoUnicornior2", new EIngredient {Proportion = 0, Quantity = 12, Type = EIngredient.TypeIngredient.Solid}},
                    {"btnTierraDeCementerior2", new EIngredient {Proportion = 0, Quantity = 600, Type = EIngredient.TypeIngredient.Solid}}
                }
            });
            loRecetasr2.Add(new ERecipe
            {
                Dosage = 4,
                Mix = new Dictionary<string, EIngredient>
                {
                    {"btnHadaNegrar2", new EIngredient {Proportion = 0, Quantity = 80, Type = EIngredient.TypeIngredient.Solid}},
                    {"btnCapaDeMerilinr2", new EIngredient {Proportion = 0, Quantity = 88, Type = EIngredient.TypeIngredient.Solid}},
                    {"EsenciaDeClavelr2", new EIngredient {Color = new Color32 (0x4A, 0x5A, 0x5B, 0xFF), Proportion = 0, Quantity = 160, Type = EIngredient.TypeIngredient.Liquid}},
                    {"btnCuernoUnicornior2", new EIngredient {Proportion = 0, Quantity = 280, Type = EIngredient.TypeIngredient.Solid}},
                    {"SalivaPieGrander2", new EIngredient {Color = new Color32 (0x4A, 0x5A, 0x5B, 0xFF), Proportion = 0, Quantity = 60, Type = EIngredient.TypeIngredient.Liquid}},
                }

            });

            #endregion
        }

        private void MezclarIngredientes()
        {
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);

            goMezclar2.SetActive(false);
            goRecetar2.SetActive(false);
            goPocionesr2.SetActive(true);

            goPocionesr2.transform.Find("btnPocionr2" + numPocionActual).GetComponent<Button>().enabled = false;

            ingredienteActual = string.Empty;
            numIngredientesServidos = 0;
            numPocionActual = 0;
        }

        [UsedImplicitly]
        private void MostrarGloboProfesorr2()
        {
            goGloboProfesorr2.SetActive(true);
            Texto = goGloboProfesorr2.transform.Find("txtGloboProfesorr2").GetComponent<Text>();
            SetTextToAnimate("Toca la puerta para iniciar el curso de Brebajes y pociones.", 4.0f);
        }

        private void MostrarLaboratorio()
        {
            goLaboratorior2.SetActive(true);
            goInicio.SetActive(false);

            GenericUi.ShowInfoPopUp(
                "Prueba 1: vencer al Minotauro.\nSelecciona y prepara las pociones, indica los gramos o mililitros que corresponden a la dosis necesaria para que funcionen las pociones.",
                new ESequence
                {
                    Action = (Challenge.Action)Action.ShowEnemy,
                    ChallengeNum = 1,
                    Event = Event.ClosePopUp
                }, 0.0f
            );
        }

        private void MostrarReceta(int numPocion)
        {
            numPocionActual = numPocion;
            goPocionesr2.SetActive(false);
            goRecetar2.SetActive(true);

            switch (numPocion)
            {
                #region Recetas

                case 1:
                    txtRecetar2.text =
                        "<size=24><b>TAURUS:</b></size> Hace que desaparezca la parte de toro.\n\n" +
                        "Para <b>una</b> dosis:\n" +
                        "	400 gr polvo de capota de torero \n" +
                        "	40 ml de agua del río que canta \n" +
                        "	28 gr de pezuña de toro \n" +
                        "	80 ml de esencia de clavel \n" +
                        "	100 gr de polvo del laberinto sin fin \n\n" +
                        "<b>PARA QUE FUNCIONE EN EL MINOTAURO NECESITAS: 2 DOSIS.</b>";
                    break;
                case 2:
                    txtRecetar2.text =
                        "<size=24><b>IMPOTENS:</b></size> : Quita la fuerza.\n\n" +
                        "Para <b>una</b> dosis:\n" +
                        "	250 gr de polvo de Hada Negra \n" +
                        "	100 gr del cabello de Sansón \n" +
                        "	95 ml llanto de Hércules \n" +
                        "	130 ml de saliva de Pie Grande \n\n" +
                        "<b>PARA QUE FUNCIONE EN EL MINOTAURO NECESITAS: 5 DOSIS.</b>";
                    break;
                case 3:
                    txtRecetar2.text =
                        "<size=24><b> TARDUS:</b></size> Aletarga al oponente.\n\n" +
                        "Para <b>una</b> dosis:\n" +
                        "	45 ml de saliva de caracol\n" +
                        "	300 gr de laberinto sin fin\n" +
                        "	120 ml de agua estancada del pozo de la perdición\n" +
                        "	12 gr de cuerno de Unicornio molido\n" +
                        "	600 gr de tierra de cementerio \n" +
                        "<b>PARA QUE FUNCIONE EN EL MINOTAURO NECESITAS: 3 DOSIS.</b>";
                    break;
                case 4:
                    txtRecetar2.text =
                        "<size=24><b> DEPELLENDAM:</b></size> Desvanece al enemigo cuando se encuentra débil.\n" +
                        "Para <b>una</b> dosis:\n" +
                        "	<size=20>80 gr de polvo de Hada Negra\n" +
                        "	88 gr de tela de la capa de Merlín\n" +
                        "	160 ml de esencia de clavel\n" +
                        "	280 gr de cuerno de Unicornio molido</size>\n" +
                        "<b>PARA QUE FUNCIONE EN EL MINOTAURO NECESITAS: 4 DOSIS.</b>";
                    break;

                    #endregion
            }

            foreach (string ingrediente in loRecetasr2[numPocionActual - 1].Mix.Keys)
            {
                goIngredientesr2.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSpriter2").GetComponent<Image>().enabled = true;
                goIngredientesr2.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillor2").GetComponent<Image>().enabled = true;
            }

            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
        }

        private void SumarProporcion(int cantidad)
        {
            if (((cantidad == 0 && txtProporcionSolidor2.text != "0") || cantidad != 0) &&
                txtProporcionSolidor2.text.Length < 8)
            {
                proporcionSolido += cantidad;
                txtProporcionSolidor2.text = proporcionSolido;
                GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
            }
        }

        private void ZoomIn(string ingrediente)
        {
            if (numPocionActual > 0 && loRecetasr2[numPocionActual - 1].Mix.ContainsKey(ingrediente))
            {
                esZoomInTerminado = false;
                ingredienteActual = ingrediente;
                goIngredientesr2.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = true;
                animZoom = goIngredientesr2.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>();
                animZoom.Play(ingrediente, -1, 0f);
                animZoom.speed = 1f;
                GenericUi.SoundManager.PlaySound("audio-brebajes-y-pociones/zoom", 1);
            }
        }

        private void ZoomOut(string ingrediente)
        {
            esZoomInTerminado = true;

            if (loRecetasr2[numPocionActual - 1].Mix[ingrediente].Type == EIngredient.TypeIngredient.Liquid)
            {
                imgAreaInteractiva.color = loRecetasr2[numPocionActual - 1].Mix[ingrediente].Color;
                goMedidorLiquidosr2.SetActive(true);
            }
            else
                goMedidorSolidosr2.SetActive(true);
        }

        #endregion

        #region Protected

        /// <summary>
        /// Execute a serie of action/event of the sequence, after an specified time
        /// </summary>
        /// <param name="sequence">The action/event to execute</param>
        /// <param name="waitingTime">Waiting time to launch the action/event of the sequence</param>
        /// <returns></returns>
        internal override IEnumerator Execute(ESequence sequence, float waitingTime)
        {
            yield return new WaitForSeconds(waitingTime);

            switch (sequence.Event)
            {
                case Event.ClosePopUp:
                    ClosePopUp((Action)sequence.Action);
                    break;
            }
        }

        /// <summary>
        /// Set the challange to its initial state
        /// </summary>
        protected override void ReStart()
        {
            #region Reset objects to initial state

            foreach (string ingrediente in lsIngredientesr2)
            {
                goIngredientesr2.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = false;
                goIngredientesr2.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>().speed = 0f;
                goIngredientesr2.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSpriter2").GetComponent<Image>().enabled = false;
                goIngredientesr2.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillor2").GetComponent<Image>().enabled = false;
            }

            goPocionesr2.transform.Find("btnPocion1r2").GetComponent<Button>().enabled = true;
            goPocionesr2.transform.Find("btnPocion2r2").GetComponent<Button>().enabled = true;
            goPocionesr2.transform.Find("btnPocion3r2").GetComponent<Button>().enabled = true;
            goPocionesr2.transform.Find("btnPocion4r2").GetComponent<Button>().enabled = true;

            txtProporcionSolidor2.text = "0";
            txtRecetar2.text = string.Empty;

            goMedidorLiquidosr2.SetActive(false);
            goMedidorSolidosr2.SetActive(false);
            goZonaBloqueor2.SetActive(true);
            goMezclar2.SetActive(false);
            goRecetar2.SetActive(false);
            goPocionesr2.SetActive(true);
            goEnemigor2.SetActive(false);
            goLaboratorior2.SetActive(false);
            goGloboProfesorr2.SetActive(false);
            goProfesorr2.SetActive(false);
            goInicio.SetActive(true);

            animPuerta.speed = 0f;

            esRetoActivo = false;
            esZoomInTerminado = false;
            sldMedidorLiquidosr2.value = 0;
            numIngredientesServidos = 0;
            numPocionActual = 0;
            ingredienteActual = string.Empty;
            proporcionSolido = string.Empty;
            InicializarPociones();

            #endregion
        }

        /// <summary>
        /// Set the state of the sequence
        /// </summary>
        /// <param name="state">The new state of the sequence</param>
        internal override void SetState(State state)
        {
            switch (state)
            {
                case State.Initial:
                    ReStart();
                    break;
                case State.Playing:
                    break;
                case State.Finished:
                    break;
            }
        }

        #endregion

        #endregion
    }
}