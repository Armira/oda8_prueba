﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
	public class Oda08Reto1 : Challenge
	{
		#region Enumeration

		public new enum Action
		{
			ShowEnemy, ShowProfessor
		}

		#endregion

		#region Attributes

		private Animator animPuerta, animZoom,animMezclaBuena,animMezclaMala;
		private bool esRetoActivo, esZoomInTerminado;
		private Button btnPuerta, btnPocion1, btnPocion2, btnPocion3, btnPocion4, btnMezclar, btnAguaCactus, btnAguaManantial, btnAguaPozo, btnAguaTriangulo, btnAlasUnicornio, btnAureola, btnCorteza, btnEspina, btnLagrimas, btnLimadura, btnMaderaBarco, btnPlumaAveztruz, btnSalivaCaracol, btnSalivaTroll, btnTierraTunel, btnUniaCorrecaminos, btnUniaLeon, btnAceptarLiquidos, btnAceptarSolidos, btnBorrarSolidos, btnTeclado0, btnTeclado1, btnTeclado2, btnTeclado3, btnTeclado4, btnTeclado5, btnTeclado6, btnTeclado7, btnTeclado8, btnTeclado9;
		private UnityEngine.GameObject goInicio,goInterfaz, goProfesor, goGloboProfesor, goLaboratorio, goEnemigo, goPociones, goReceta, goMezcla, goZonaPreparacion, goIngredientes, goZonaBloqueo, goMedidorLiquidos, goMedidorSolidos;
		private Image imgInterfaz, imgReceta, imgAreaInteractiva;
		private int numPocionActual, numIngredientesServidos;
		private List<string> lsIngredientes;
		private List<ERecipe> loRecetas;
		private Slider sldMedidorLiquidos;
		private string ingredienteActual, proporcionSolido;
		private Text txtReceta, txtProporcionSolido;

		#endregion

		#region Constructor

		public Oda08Reto1() : base("Reto1")
		{
			
		}

		#endregion

		#region Events

		#region Overridden

		// Use this for initialization
		protected override void Start()
		{
			#region Object instances

			lsIngredientes = new List<string> {
				"AguaCactus", "AguaManantial", "AguaPozo", "AguaTriangulo", "AlasUnicornio", "Aureola",
				"Corteza", "Espina", "Lagrimas", "Limadura", "MaderaBarco", "PlumaAveztruz", "SalivaCaracol",
				"SalivaTroll", "TierraTunel", "UniaCorrecaminos", "UniaLeon"
			};

			goInicio = Canvas.transform.Find("Inicio").gameObject;
			goLaboratorio = Canvas.transform.Find("Laboratorio").gameObject;

			btnPuerta = goInicio.transform.Find("btnPuerta").GetComponent<Button>();
			goProfesor = goInicio.transform.Find("Profesor").gameObject;

			goEnemigo = goLaboratorio.transform.Find("Enemigo").gameObject;

			animPuerta = btnPuerta.GetComponent<Animator>();
			animPuerta.speed = 0f;

			goGloboProfesor = goProfesor.transform.Find("GloboProfesor").gameObject;

			imgInterfaz = goEnemigo.transform.Find("imgInterfaz").GetComponent<Image>();
			goZonaPreparacion = goEnemigo.transform.Find("ZonaPreparacion").gameObject;

			goPociones = imgInterfaz.transform.Find("Pociones").gameObject;
			goReceta = imgInterfaz.transform.Find("Receta").gameObject;
			goMezcla = imgInterfaz.transform.Find("Mezcla").gameObject;

			animMezclaBuena = goMezcla.transform.Find("imgMezclaBuena").GetComponent< Animator>();

			btnPocion1 = goPociones.transform.Find("btnPocion1").GetComponent<Button>();
			btnPocion2 = goPociones.transform.Find("btnPocion2").GetComponent<Button>();
			btnPocion3 = goPociones.transform.Find("btnPocion3").GetComponent<Button>();
			btnPocion4 = goPociones.transform.Find("btnPocion4").GetComponent<Button>();

			btnMezclar = goMezcla.transform.Find("btnMezclar").GetComponent<Button>();

			imgReceta = goReceta.transform.Find("imgReceta").GetComponent<Image>();

			txtReceta = imgReceta.transform.Find("txtReceta").GetComponent<Text>();

			goIngredientes = goZonaPreparacion.transform.Find("Ingredientes").gameObject;
			goZonaBloqueo = goZonaPreparacion.transform.Find("ZonaBloqueo").gameObject;

			btnAguaCactus = goIngredientes.transform.Find("btnAguaCactus").GetComponent<Button>();
			btnAguaManantial = goIngredientes.transform.Find("btnAguaManantial").GetComponent<Button>();
			btnAguaPozo = goIngredientes.transform.Find("btnAguaPozo").GetComponent<Button>();
			btnAguaTriangulo = goIngredientes.transform.Find("btnAguaTriangulo").GetComponent<Button>();
			btnAlasUnicornio = goIngredientes.transform.Find("btnAlasUnicornio").GetComponent<Button>();
			btnAureola = goIngredientes.transform.Find("btnAureola").GetComponent<Button>();
			btnCorteza = goIngredientes.transform.Find("btnCorteza").GetComponent<Button>();
			btnEspina = goIngredientes.transform.Find("btnEspina").GetComponent<Button>();
			btnLagrimas = goIngredientes.transform.Find("btnLagrimas").GetComponent<Button>();
			btnLimadura = goIngredientes.transform.Find("btnLimadura").GetComponent<Button>();
			btnMaderaBarco = goIngredientes.transform.Find("btnMaderaBarco").GetComponent<Button>();
			btnPlumaAveztruz = goIngredientes.transform.Find("btnPlumaAveztruz").GetComponent<Button>();
			btnSalivaCaracol = goIngredientes.transform.Find("btnSalivaCaracol").GetComponent<Button>();
			btnSalivaTroll = goIngredientes.transform.Find("btnSalivaTroll").GetComponent<Button>();
			btnTierraTunel = goIngredientes.transform.Find("btnTierraTunel").GetComponent<Button>();
			btnUniaCorrecaminos = goIngredientes.transform.Find("btnUniaCorrecaminos").GetComponent<Button>();
			btnUniaLeon = goIngredientes.transform.Find("btnUniaLeon").GetComponent<Button>();

			foreach (string ingrediente in lsIngredientes)
			{
				goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = false;
				goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>().speed = 0f;
				goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = false;
				goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = false;
			}

			goMedidorLiquidos = goZonaPreparacion.transform.Find("MedidorLiquidos").gameObject;
			goMedidorSolidos = goZonaPreparacion.transform.Find("MedidorSolidos").gameObject;

			btnAceptarLiquidos = goMedidorLiquidos.transform.Find("btnAceptar").GetComponent<Button>();
			sldMedidorLiquidos = goMedidorLiquidos.transform.Find("imgProbeta").GetComponent<Image>().transform.Find("sldMedidor").GetComponent<Slider>();

			imgAreaInteractiva = sldMedidorLiquidos.GetComponentsInChildren<Image>().FirstOrDefault(item => item.name == "imgAreaInteractiva");

			txtProporcionSolido = goMedidorSolidos.transform.Find("imgBascula").GetComponent<Image>().transform.Find("txtBascula").GetComponent<Text>();
			btnAceptarSolidos = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnAceptar").GetComponent<Button>();
			btnBorrarSolidos = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnBorrar").GetComponent<Button>();
			btnTeclado0 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla0").GetComponent<Button>();
			btnTeclado1 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla1").GetComponent<Button>();
			btnTeclado2 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla2").GetComponent<Button>();
			btnTeclado3 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla3").GetComponent<Button>();
			btnTeclado4 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla4").GetComponent<Button>();
			btnTeclado5 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla5").GetComponent<Button>();
			btnTeclado6 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla6").GetComponent<Button>();
			btnTeclado7 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla7").GetComponent<Button>();
			btnTeclado8 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla8").GetComponent<Button>();
			btnTeclado9 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla9").GetComponent<Button>();

			InicializarPociones();

			#endregion

			#region Event listeners

			btnPuerta.onClick.AddListener(MostrarLaboratorio);
			btnPocion1.onClick.AddListener(delegate
			{
				MostrarReceta(1);
				HabilitarZonaPreparacion();
			});
			btnPocion2.onClick.AddListener(delegate
			{
				MostrarReceta(2);
				HabilitarZonaPreparacion();
			});
			btnPocion3.onClick.AddListener(delegate
			{
				MostrarReceta(3);
				HabilitarZonaPreparacion();
			});
			btnPocion4.onClick.AddListener(delegate
			{
				MostrarReceta(4);
				HabilitarZonaPreparacion();
			});
			btnMezclar.onClick.AddListener(MezclarIngredientes);
			btnAguaCactus.onClick.AddListener(delegate
			{
				ZoomIn("AguaCactus");
			});
			btnAguaManantial.onClick.AddListener(delegate
			{
				ZoomIn("AguaManantial");
			});
			btnAguaPozo.onClick.AddListener(delegate
			{
				ZoomIn("AguaPozo");
			});
			btnAguaTriangulo.onClick.AddListener(delegate
			{
				ZoomIn("AguaTriangulo");
			});
			btnAlasUnicornio.onClick.AddListener(delegate
			{
				ZoomIn("AlasUnicornio");
			});
			btnAureola.onClick.AddListener(delegate
			{
				ZoomIn("Aureola");
			});
			btnCorteza.onClick.AddListener(delegate
			{
				ZoomIn("Corteza");
			});
			btnEspina.onClick.AddListener(delegate
			{
				ZoomIn("Espina");
			});
			btnLagrimas.onClick.AddListener(delegate
			{
				ZoomIn("Lagrimas");
			});
			btnLimadura.onClick.AddListener(delegate
			{
				ZoomIn("Limadura");
			});
			btnMaderaBarco.onClick.AddListener(delegate
			{
				ZoomIn("MaderaBarco");
			});
			btnPlumaAveztruz.onClick.AddListener(delegate
			{
				ZoomIn("PlumaAveztruz");
			});
			btnSalivaCaracol.onClick.AddListener(delegate
			{
				ZoomIn("SalivaCaracol");
			});
			btnSalivaTroll.onClick.AddListener(delegate
			{
				ZoomIn("SalivaTroll");
			});
			btnTierraTunel.onClick.AddListener(delegate
			{
				ZoomIn("TierraTunel");
			});
			btnUniaCorrecaminos.onClick.AddListener(delegate
			{
				ZoomIn("UniaCorrecaminos");
			});
			btnUniaLeon.onClick.AddListener(delegate
			{
				ZoomIn("UniaLeon");
			});
			btnAceptarLiquidos.onClick.AddListener(GuardarProporcion);
			btnAceptarSolidos.onClick.AddListener(GuardarProporcion);
			btnBorrarSolidos.onClick.AddListener(BorrarProporcion);
			btnTeclado0.onClick.AddListener(delegate
			{
				SumarProporcion(0);
			});
			btnTeclado1.onClick.AddListener(delegate
			{
				SumarProporcion(1);
			});
			btnTeclado2.onClick.AddListener(delegate
			{
				SumarProporcion(2);
			});
			btnTeclado3.onClick.AddListener(delegate
			{
				SumarProporcion(3);
			});
			btnTeclado4.onClick.AddListener(delegate
			{
				SumarProporcion(4);
			});
			btnTeclado5.onClick.AddListener(delegate
			{
				SumarProporcion(5);
			});
			btnTeclado6.onClick.AddListener(delegate
			{
				SumarProporcion(6);
			});
			btnTeclado7.onClick.AddListener(delegate
			{
				SumarProporcion(7);
			});
			btnTeclado8.onClick.AddListener(delegate
			{
				SumarProporcion(8);
			});
			btnTeclado9.onClick.AddListener(delegate
			{
				SumarProporcion(9);
			});

			#endregion
		}

		// Update is called once per frame
		protected override void Update()
		{
			#region Initial state

			if (Instance.activeInHierarchy)
			{
				if (!esRetoActivo)
				{
					GenericUi.ShowInfoPopUp(
						"¡La escuela de magia Kadabra, se alegra de tenerte entre sus estudiantes! Deberás esforzarte porque el profesor Galaguer es uno de los más exigentes",
						new ESequence {
							Action = (Challenge.Action) Action.ShowProfessor,
							ChallengeNum = 1,
							Event = Event.ClosePopUp
						}, 0.0f
					);
					esRetoActivo = true;
				}
				else
				{
					if (!string.IsNullOrEmpty(ingredienteActual) && !esZoomInTerminado &&
						animZoom.GetCurrentAnimatorStateInfo(0).IsName(ingredienteActual) &&
						animZoom.GetCurrentAnimatorStateInfo(0).normalizedTime % animZoom.GetCurrentAnimatorStateInfo(0).length  > 1.0f)
						ZoomOut(ingredienteActual);
				}
			}
			else
				esRetoActivo = false;

			#endregion

			AnimateText();
		}

		#endregion

		#region Private

		private void ClosePopUp(Action action)
		{
			switch (action)
			{
				#region Cases

				case Action.ShowEnemy:
					goEnemigo.SetActive(true);
					break;
				case Action.ShowProfessor:
					goProfesor.SetActive(true);
					animPuerta.Play("Puerta");
					animPuerta.speed = 1f;
					Invoke("MostrarGloboProfesor", 0.45f);
					break;

				#endregion
			}

			GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
		}

		#endregion

		#endregion

		#region Methods

		#region Private

		private void BorrarProporcion()
		{
			proporcionSolido = string.Empty;
			txtProporcionSolido.text = "0";
			GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
		}

		private void ContabilizarIngredienteServido()
		{
			goIngredientes.transform.Find("btn" + ingredienteActual).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = false;
			goIngredientes.transform.Find("btn" + ingredienteActual).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = false;
			GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);

			if (++numIngredientesServidos == loRecetas[numPocionActual - 1].Mix.Count)
				goMezcla.SetActive(true);
		}

		private void GuardarProporcion()
		{
			if (loRecetas[numPocionActual - 1].Mix[ingredienteActual].Type == EIngredient.TypeIngredient.Liquid)
			{
				#region Guardar proporción de ingrediente líquido
				
				if ((int)sldMedidorLiquidos.value > 0)
				{
					loRecetas[numPocionActual - 1].Mix[ingredienteActual].Proportion = Mathf.RoundToInt(sldMedidorLiquidos.value);
					Debug.Log("Valor de liquido: "+ Mathf.RoundToInt(sldMedidorLiquidos.value));
					sldMedidorLiquidos.value = 0;
					ContabilizarIngredienteServido();
				}
				goMedidorLiquidos.SetActive(false);

				#endregion
			}
			else
			{
				#region Guardar proporción de ingrediente sólido

				if (!string.IsNullOrEmpty(proporcionSolido))
				{
					int proporcion = int.Parse(proporcionSolido);

					if (proporcion != 0)
					{
						loRecetas[numPocionActual - 1].Mix[ingredienteActual].Proportion = proporcion;
						ContabilizarIngredienteServido();
					}
					Debug.Log("porcion de solido: "+proporcion);
					proporcionSolido = string.Empty;
					txtProporcionSolido.text = "0";
				}

				goMedidorSolidos.SetActive(false);

				#endregion
			}
		}

		private void HabilitarZonaPreparacion()
		{
			goZonaBloqueo.SetActive(false);
			GenericUi.Resume();
		}

		private void InicializarPociones()
		{
			//ReSharper disable once UseObjectOrCollectionInitializer
			loRecetas = new List<ERecipe>();

			#region Initializing potion mix


			loRecetas.Add(new ERecipe
			{
				Dosage = 2,
				Mix = new Dictionary<string, EIngredient>
				{
					{"Corteza", new EIngredient {Proportion = 0, Quantity = 200, Type = EIngredient.TypeIngredient.Solid}},
					{"AguaCactus", new EIngredient {Color = new Color32(0x79, 0xD0, 0xD3, 0xFF), Proportion = 0, Quantity = 10, Type = EIngredient.TypeIngredient.Liquid}},
					{"PlumaAveztruz", new EIngredient {Proportion = 0, Quantity = 2, Type = EIngredient.TypeIngredient.Solid}},
					{"SalivaCaracol", new EIngredient {Color = new Color32(0x99, 0xE5, 0xA3, 0xFF), Proportion = 0, Quantity = 5, Type = EIngredient.TypeIngredient.Liquid}},
					{"Aureola", new EIngredient {Proportion = 0, Quantity = 100, Type = EIngredient.TypeIngredient.Solid}}
				}

			});
			loRecetas.Add(new ERecipe
			{
				Dosage = 4,
				Mix = new Dictionary<string, EIngredient>
				{
					{"Limadura", new EIngredient {Proportion = 0, Quantity = 50, Type = EIngredient.TypeIngredient.Solid}},
					{"UniaLeon", new EIngredient {Proportion = 0, Quantity = 10, Type = EIngredient.TypeIngredient.Solid}},
					{"AguaManantial", new EIngredient {Color = new Color32(0x04, 0xF0, 0xFF, 0xFF), Proportion = 0, Quantity = 15, Type = EIngredient.TypeIngredient.Liquid}},
					{"SalivaTroll", new EIngredient {Color = new Color32(0x00, 0xED, 0x00, 0xFF), Proportion = 0, Quantity = 30, Type = EIngredient.TypeIngredient.Liquid}}
				}

			});
			loRecetas.Add(new ERecipe
			{
				Dosage = 3,
				Mix = new Dictionary<string, EIngredient>
				{
					{"Espina", new EIngredient {Proportion = 0, Quantity = 10, Type = EIngredient.TypeIngredient.Solid}},
					{"Corteza", new EIngredient {Proportion = 0, Quantity = 400, Type = EIngredient.TypeIngredient.Solid}},
					{"Lagrimas", new EIngredient {Color = new Color32(0x16, 0xD2, 0xD9, 0xFF), Proportion = 0, Quantity = 25, Type = EIngredient.TypeIngredient.Liquid}},
					{"AguaPozo", new EIngredient {Color = new Color32(0x2B, 0x3A, 0x3B, 0xFF), Proportion = 0, Quantity = 20, Type = EIngredient.TypeIngredient.Liquid}},
					{"TierraTunel", new EIngredient {Proportion = 0, Quantity = 200, Type = EIngredient.TypeIngredient.Solid}}
				}

			});
			loRecetas.Add(new ERecipe
			{
				Dosage = 2,
				Mix = new Dictionary<string, EIngredient>
				{
					{"UniaCorrecaminos", new EIngredient {Proportion = 0, Quantity = 10, Type = EIngredient.TypeIngredient.Solid}},
					{"AlasUnicornio", new EIngredient {Proportion = 0, Quantity = 3, Type = EIngredient.TypeIngredient.Solid}},
					{"AguaTriangulo", new EIngredient {Color = new Color32(0x4A, 0x5A, 0x5B, 0xFF), Proportion = 0, Quantity = 30, Type = EIngredient.TypeIngredient.Liquid}},
					{"MaderaBarco", new EIngredient {Proportion = 0, Quantity = 200, Type = EIngredient.TypeIngredient.Solid}}
				}

			});

			#endregion
		}

		private void MezclarIngredientes()
		{
			GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);

			goMezcla.SetActive(false);
			goReceta.SetActive(false);
			goPociones.SetActive(true);

			goPociones.transform.Find("btnPocion" + numPocionActual).GetComponent<Button>().enabled = false;

			ingredienteActual = string.Empty;
			numIngredientesServidos = 0;
			numPocionActual = 0;

            AnimationIngredient();

		}


        private void AnimationIngredient()
        {


			imgInterfaz.transform.Find ("AnimationMezcla").gameObject.SetActive(true);





			animMezclaBuena.Play ("AnimacionBien");

        }


		[UsedImplicitly]
		private void MostrarGloboProfesor()
		{
			goGloboProfesor.SetActive(true);
			Texto = goGloboProfesor.transform.Find("txtGloboProfesor").GetComponent<Text>();
			SetTextToAnimate("Toca la puerta para iniciar el curso de Brebajes y pociones.", 4.0f);
		}

		private void MostrarLaboratorio()
		{
			goLaboratorio.SetActive(true);
			goInicio.SetActive(false);

			GenericUi.ShowInfoPopUp(
				"Prueba 1: vencer al Grifo.\nSelecciona y prepara las pociones, indica los gramos o mililitros que corresponden a la dosis necesaria para que funcionen las pociones.",
				new ESequence {
					Action = (Challenge.Action)Action.ShowEnemy,
					ChallengeNum = 1,
					Event = Event.ClosePopUp
				}, 0.0f
			);
		}

		private void MostrarReceta(int numPocion)
		{
			numPocionActual = numPocion;
			goPociones.SetActive(false);
			goReceta.SetActive(true);

			switch (numPocion)
			{
				#region Recetas

				case 1:
					txtReceta.text = 
						"<size=24><b>APTERA:</b></size> Evita volar.\n\n" +
						"Para <b>una</b> dosis:\n" +
						"	200 gr de corteza de árbol llorón\n" +
						"	10 ml de agua de cactus\n" +
						"	2 gr de plumas de avestruz\n" +
						"	5 ml de saliva de caracol\n" +
						"	100 gr de aureola de ángel caído\n\n" +
						"<b>PARA QUE FUNCIONE EN EL GRIFO NECESITAS: 2 DOSIS.</b>";
					break;
				case 2:
					txtReceta.text = 
						"<size=24><b>UNGUIBUS:</b></size> Evita que use sus garras.\n\n" +
						"Para <b>una</b> dosis:\n" +
						"	50 gr de lija celestial\n" +
						"	10 gr de uña molida de león\n" +
						"	15 ml de agua del gran manantial\n" +
						"	30 ml de saliva ácida de troll\n\n" +
						"<b>PARA QUE FUNCIONE EN EL GRIFO NECESITAS: 4 DOSIS.</b>";
					break;
				case 3:
					txtReceta.text = 
						"<size=24><b>CAVAEM:</b></size> Encierra en una prisión invisible.\n\n" +
						"Para <b>una</b> dosis:\n" +
						"	10 gr de espinas de puercoespín\n" +
						"	400 gr de corteza de árbol llorón\n" +
						"	25 ml de lágrimas del eterno prisionero\n" +
						"	20 ml de agua del pozo oscuro\n" +
						"	200 gr de tierra del túnel sin salida\n" +
						"<b>PARA QUE FUNCIONE EN EL GRIFO NECESITAS: 3 DOSIS.</b>";
					break;
				case 4:
					txtReceta.text = 
						"<size=24><b>TRANSPORT:</b></size> Desaparece al enemigo y lo hace reaparecer en el lugar que pienses.\n" +
						"Para <b>una</b> dosis:\n" +
						"	<size=20>10 gr de uña molida de correcaminos\n" +
						"	3 gr de alas de unicornio\n" +
						"	30 ml de agua del triángulo de las Bermudas\n" +
						"	200 gr de madera del barco de Barba Negra</size>\n" +
						"<b>PARA QUE FUNCIONE EN EL GRIFO NECESITAS: 2 DOSIS.</b>";
					break;

				#endregion
			}

			foreach (string ingrediente in loRecetas[numPocionActual - 1].Mix.Keys)
			{
				goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = true;
				goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = true;
				Debug.Log ("ingrediente "+ingrediente);
				Debug.Log ("proporcion "+loRecetas[numPocionActual - 1].Mix[ingrediente].Proportion);
				Debug.Log ("cantaidad "+loRecetas[numPocionActual - 1].Mix[ingrediente].Quantity);

			}

			GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
		}

		private void SumarProporcion(int cantidad)
		{
			if (((cantidad == 0 && txtProporcionSolido.text != "0") || cantidad != 0) && 
				txtProporcionSolido.text.Length < 8)
			{
				proporcionSolido += cantidad;
				txtProporcionSolido.text = proporcionSolido;
				GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
			}
		}

		private void ZoomIn(string ingrediente)
		{
			if (numPocionActual > 0 && loRecetas[numPocionActual - 1].Mix.ContainsKey(ingrediente))
			{
				esZoomInTerminado = false;
				ingredienteActual = ingrediente;
				goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = true;
				animZoom = goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>();
				animZoom.Play(ingrediente, -1, 0f);
				animZoom.speed = 1f;
				GenericUi.SoundManager.PlaySound("audio-brebajes-y-pociones/zoom", 1);
			}
		}

		private void ZoomOut(string ingrediente)
		{
			esZoomInTerminado = true;

			if (loRecetas[numPocionActual - 1].Mix[ingrediente].Type == EIngredient.TypeIngredient.Liquid)
			{
				imgAreaInteractiva.color = loRecetas[numPocionActual - 1].Mix[ingrediente].Color;
				goMedidorLiquidos.SetActive(true);
			}
			else
				goMedidorSolidos.SetActive(true);
		}

		#endregion

		#region Protected

		/// <summary>
		/// Execute a serie of action/event of the sequence, after an specified time
		/// </summary>
		/// <param name="sequence">The action/event to execute</param>
		/// <param name="waitingTime">Waiting time to launch the action/event of the sequence</param>
		/// <returns></returns>
		internal override IEnumerator Execute(ESequence sequence, float waitingTime)
		{
			yield return new WaitForSeconds(waitingTime);

			switch (sequence.Event)
			{
				case Event.ClosePopUp:
					ClosePopUp((Action)sequence.Action);
					break;
			}
		}

		/// <summary>
		/// Set the challange to its initial state
		/// </summary>
		protected override void ReStart()
		{
			#region Reset objects to initial state

			foreach (string ingrediente in lsIngredientes)
			{
				goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = false;
				goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>().speed = 0f;
				goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = false;
				goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = false;
			}

			goPociones.transform.Find("btnPocion1").GetComponent<Button>().enabled = true;
			goPociones.transform.Find("btnPocion2").GetComponent<Button>().enabled = true;
			goPociones.transform.Find("btnPocion3").GetComponent<Button>().enabled = true;
			goPociones.transform.Find("btnPocion4").GetComponent<Button>().enabled = true;

			txtProporcionSolido.text = "0";
			txtReceta.text = string.Empty;
			
			goMedidorLiquidos.SetActive(false);
			goMedidorSolidos.SetActive(false);
			goZonaBloqueo.SetActive(true);
			goMezcla.SetActive(false);
			goReceta.SetActive(false);
			goPociones.SetActive(true);
			goEnemigo.SetActive(false);
			goLaboratorio.SetActive(false);
			goGloboProfesor.SetActive(false);
			goProfesor.SetActive(false);
			goInicio.SetActive(true);

			animPuerta.speed = 0f;

			esRetoActivo = false;
			esZoomInTerminado = false;
			sldMedidorLiquidos.value = 0;
			numIngredientesServidos = 0;
			numPocionActual = 0;
			ingredienteActual = string.Empty;
			proporcionSolido = string.Empty;
			InicializarPociones();

			#endregion
		}

		/// <summary>
		/// Set the state of the sequence
		/// </summary>
		/// <param name="state">The new state of the sequence</param>
		internal  override void SetState(State state)
		{
			switch (state)
			{
				case State.Initial:
					ReStart();
					break;
				case State.Playing:
					break;
				case State.Finished:
					break;
			}
		}

		#endregion

		#endregion
	}
}