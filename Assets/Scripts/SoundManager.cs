﻿using JetBrains.Annotations;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
	public class SoundManager : MonoBehaviour
	{
		#region Attributes

		[HideInInspector]
		private AudioSource[] asAudioSources;

		#endregion

		#region Events

		#region Private

		// Use this for initialization
		[UsedImplicitly]
		private void Start()
		{
			asAudioSources = GetComponents<AudioSource>();
			asAudioSources[0].volume = 0.5f;
			asAudioSources[0].loop = true;
			asAudioSources[1].volume = 1.0f;
		}

		private IEnumerator NextSound(string soundName, int audioSourceIndex)
		{
#if UNITY_EDITOR
			var filePath = Application.streamingAssetsPath;
#elif UNITY_IOS
			var filePath = Application.persistentDataPath;
#elif UNITY_ANDROID
			var filePath = Application.persistentDataPath;
#endif

			var audioFile = new WWW("file://" + filePath + "/" + soundName + ".mp3");

			yield return audioFile;

			#region Try to play sound

			try
			{
				asAudioSources[audioSourceIndex].Stop();
				asAudioSources[audioSourceIndex].clip = audioFile.GetAudioClip(true, true, AudioType.MPEG);
				asAudioSources[audioSourceIndex].Play();
			}
			catch
			{
				Debug.Log("Fallo al reproducir el sonido");
			}

			#endregion
		}

		#endregion

		#region Public

		/// <summary>
		/// Play a sound in an audio-source
		/// </summary>
		/// <param name="soundName">The sound name</param>
		/// <param name="audioSource">The audio-source index to play</param>
		public void PlaySound(string soundName, int audioSource)
		{
			StartCoroutine(NextSound(soundName, audioSource));
		}

		#endregion

		#endregion
	}
}
