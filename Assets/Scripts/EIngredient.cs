﻿using UnityEngine;

namespace Assets.Scripts
{
	public class EIngredient
	{
		#region Enumeration
		public enum TypeIngredient
		{
			Liquid, Solid
		}

		#endregion

		#region Properties

		public Color Color { get; set; }

		public int Proportion { get; set; }

		public int Quantity { get; set; }

		public TypeIngredient Type { get; set; }

		#endregion
	}
}
