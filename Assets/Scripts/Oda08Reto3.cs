﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class Oda08Reto3 : Challenge
    {
        #region Enumeration

        public new enum Action
        {
            ShowEnemy, ShowProfessor
        }

        #endregion

        #region Attributes

        private Animator animPuerta, animZoom;
        private bool esRetoActivo, esZoomInTerminado;
        private Button btnPuerta, btnPocion1, btnPocion2, btnPocion3, btnPocion4, btnCorteza, btnAguaCactus, btnPlumaDeAvestruz, btnSalivaCaracol, btnAureola, btnCenizasDeFenix, btnRocaLunar, btnRocio, btnSalivaDeOgro, btnUnaDeYeti, btnRioQueCanta, btnPoloNorte, btnHadaNegra, btnCapaMerlin,
        btnEsenciaDeClavel, btnCuernoUnicornio, btnSalivaPieGrande, btnAceptarLiquidos, btnAceptarSolidos, btnBorrarSolidos, btnTeclado0, btnTeclado1, btnTeclado2, btnTeclado3, btnTeclado4, btnTeclado5, btnTeclado6, btnTeclado7, btnTeclado8, btnTeclado9;
        private UnityEngine.GameObject goInicio, goProfesor, goGloboProfesor, goLaboratorio, goEnemigo, goPociones, goReceta, goMezcla, goZonaPreparacion, goIngredientes, goZonaBloqueo, goMedidorLiquidos, goMedidorSolidos;
        private Image imgInterfaz, imgReceta, imgAreaInteractiva;
        private int numPocionActual, numIngredientesServidos;
        private List<string> lsIngredientes;
        private List<ERecipe> loRecetas;
        private Slider sldMedidorLiquidos;
        private string ingredienteActual, proporcionSolido;
        private Text txtReceta, txtProporcionSolido;
        private Button btnMezclar;
        #endregion

        #region Constructor

        public Oda08Reto3() : base("Reto3")
		{
			
		}

		#endregion

		#region Events

		#region Overridden

		// Use this for initialization
		protected override void Start()
        {
            #region Object instances

            lsIngredientes = new List<string> {
"btnCorteza", "btnAguaCactus", "btnPlumaDeAvestruz", "btnSalivaCaracol", "btnAureola", "btnCenizasDeFenix",
"btnRocaLunar", "btnRocio", "btnSalivaDeOgr", "btnUnaDeYeti", "btnRioQueCanta", "btnPoloNorte",
"btnHadaNegra", "btnCapaMerlin", "btnEsenciaDeClavel", "btnCuernoUnicornio", "btnSalivaPieGrande"
            };

            goInicio = Canvas.transform.Find("Inicio").gameObject;
            goLaboratorio = Canvas.transform.Find("Laboratorio").gameObject;

            btnPuerta = goInicio.transform.Find("btnPuerta").GetComponent<Button>();
            goProfesor = goInicio.transform.Find("Profesor").gameObject;

            goEnemigo = goLaboratorio.transform.Find("Enemigo").gameObject;

            animPuerta = btnPuerta.GetComponent<Animator>();
            animPuerta.speed = 0f;

            goGloboProfesor = goProfesor.transform.Find("GloboProfesor").gameObject;

            imgInterfaz = goEnemigo.transform.Find("imgInterfaz").GetComponent<Image>();
            goZonaPreparacion = goEnemigo.transform.Find("ZonaPreparacion").gameObject;

            goPociones = imgInterfaz.transform.Find("Pociones").gameObject;
            goReceta = imgInterfaz.transform.Find("Receta").gameObject;
            goMezcla = imgInterfaz.transform.Find("Mezcla").gameObject;

            btnPocion1 = goPociones.transform.Find("btnPocion1").GetComponent<Button>();
            btnPocion2 = goPociones.transform.Find("btnPocion2").GetComponent<Button>();
            btnPocion3 = goPociones.transform.Find("btnPocion3").GetComponent<Button>();
            btnPocion4 = goPociones.transform.Find("btnPocion4").GetComponent<Button>();

            btnMezclar = goMezcla.transform.Find("btnMezclar").GetComponent<Button>();

            imgReceta = goReceta.transform.Find("imgReceta").GetComponent<Image>();

            txtReceta = imgReceta.transform.Find("txtReceta").GetComponent<Text>();

            //goIngredientes = goZonaPreparacion.transform.Find("Ingredientesr3").gameObject;
            goZonaBloqueo = goZonaPreparacion.transform.Find("ZonaBloqueor3").gameObject;

            btnCorteza = goIngredientes.transform.Find("btnCorteza").GetComponent<Button>();
            btnAguaCactus = goIngredientes.transform.Find("btnAguaCactus").GetComponent<Button>();
            btnPlumaDeAvestruz = goIngredientes.transform.Find("btnPlumaDeAvestruz ").GetComponent<Button>();
            btnSalivaCaracol = goIngredientes.transform.Find("btnSalivaCaracol").GetComponent<Button>();
            btnAureola = goIngredientes.transform.Find("btnAureola").GetComponent<Button>();
            btnCenizasDeFenix = goIngredientes.transform.Find("btnCenizasDeFenix").GetComponent<Button>();
            btnRocaLunar =
goIngredientes.transform.Find("btnRocaLunar").GetComponent<Button>();
            btnRocio =
goIngredientes.transform.Find("btnRocio").GetComponent<Button>();
            btnSalivaDeOgro = goIngredientes.transform.Find("btnSalivaDeOgro").GetComponent<Button>();
            btnUnaDeYeti = goIngredientes.transform.Find("btnUnaDeYeti").GetComponent<Button>();
            btnRioQueCanta =
goIngredientes.transform.Find("btnRioQueCanta").GetComponent<Button>();
            btnPoloNorte =
goIngredientes.transform.Find("btnPoloNorte").GetComponent<Button>();
            btnHadaNegra = goIngredientes.transform.Find("btnHadaNegra").GetComponent<Button>();
            btnCapaMerlin = goIngredientes.transform.Find("btnCapaDeMerlin").GetComponent<Button>();
            btnEsenciaDeClavel = goIngredientes.transform.Find("btnEsenciaDeClavel").GetComponent<Button>();
            btnCuernoUnicornio = goIngredientes.transform.Find("btnCuernoUnicornio").GetComponent<Button>();
            btnSalivaPieGrande = goIngredientes.transform.Find("btnSalivaPieGrande").GetComponent<Button>();
            foreach (string ingrediente in lsIngredientes)
            {
                goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = false;
                goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>().speed = 0f;
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = false;
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = false;
            }

            goMedidorLiquidos = goZonaPreparacion.transform.Find("MedidorLiquidos").gameObject;
            goMedidorSolidos = goZonaPreparacion.transform.Find("MedidorSolidos").gameObject;

            btnAceptarLiquidos = goMedidorLiquidos.transform.Find("btnAceptar").GetComponent<Button>();
            sldMedidorLiquidos = goMedidorLiquidos.transform.Find("imgProbeta").GetComponent<Image>().transform.Find("sldMedidor").GetComponent<Slider>();

            imgAreaInteractiva = sldMedidorLiquidos.GetComponentsInChildren<Image>().FirstOrDefault(item => item.name == "imgAreaInteractiva");

            txtProporcionSolido = goMedidorSolidos.transform.Find("imgBascula").GetComponent<Image>().transform.Find("txtBascula").GetComponent<Text>();
            btnAceptarSolidos = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnAceptar").GetComponent<Button>();
            btnBorrarSolidos = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnBorrar").GetComponent<Button>();
            btnTeclado0 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla0").GetComponent<Button>();
            btnTeclado1 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla1").GetComponent<Button>();
            btnTeclado2 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla2").GetComponent<Button>();
            btnTeclado3 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla3").GetComponent<Button>();
            btnTeclado4 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla4").GetComponent<Button>();
            btnTeclado5 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla5").GetComponent<Button>();
            btnTeclado6 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla6").GetComponent<Button>();
            btnTeclado7 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla7").GetComponent<Button>();
            btnTeclado8 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla8").GetComponent<Button>();
            btnTeclado9 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla9").GetComponent<Button>();

            InicializarPociones();

            #endregion

            #region Event listeners

            btnPuerta.onClick.AddListener(MostrarLaboratorio);
            btnPocion1.onClick.AddListener(delegate
            {
                MostrarReceta(1);
                HabilitarZonaPreparacion();
            });
            btnPocion2.onClick.AddListener(delegate
            {
                MostrarReceta(2);
                HabilitarZonaPreparacion();
            });
            btnPocion3.onClick.AddListener(delegate
            {
                MostrarReceta(3);
                HabilitarZonaPreparacion();
            });
            btnPocion4.onClick.AddListener(delegate
            {
                MostrarReceta(4);
                HabilitarZonaPreparacion();
            });
            btnMezclar.onClick.AddListener(MezclarIngredientes);


            btnCorteza.onClick.AddListener(delegate
            {
                ZoomIn("Corteza");
            });
            btnAguaCactus.onClick.AddListener(delegate
            {
                ZoomIn("AguaCactus");
            });
            btnPlumaDeAvestruz.onClick.AddListener(delegate
            {
                ZoomIn("PlumaDeAvestruz");
            });
            btnSalivaCaracol.onClick.AddListener(delegate
            {
                ZoomIn("SalivaCaracol");
            });
            btnAureola.onClick.AddListener(delegate
            {
                ZoomIn("Aureola");
            });
            btnCenizasDeFenix.onClick.AddListener(delegate
            {
                ZoomIn("CenizasDeFenix");
            });
            btnRocaLunar.onClick.AddListener(delegate
            {
                ZoomIn("RocaLunar");
            });
            btnRocio.onClick.AddListener(delegate
            {
                ZoomIn("Rocio");
            });
            btnSalivaDeOgro.onClick.AddListener(delegate
            {
                ZoomIn("SalivaDeOgro");
            });
            btnUnaDeYeti.onClick.AddListener(delegate
            {
                ZoomIn("UnaDeYeti");
            });
            btnRioQueCanta.onClick.AddListener(delegate
            {
                ZoomIn("RioQueCanta");
            });
            btnPoloNorte.onClick.AddListener(delegate
            {
                ZoomIn("PoloNorte");
            });
            btnHadaNegra.onClick.AddListener(delegate
            {
                ZoomIn("HadaNegra");
            });
            btnCapaMerlin.onClick.AddListener(delegate
            {
                ZoomIn("btnCapaDeMerilin");
            });
            btnEsenciaDeClavel.onClick.AddListener(delegate
            {
                ZoomIn("EsenciaDeClavel");
            });
            btnCuernoUnicornio.onClick.AddListener(delegate
            {
                ZoomIn("btnCuernoUnicornio");
            });
            btnSalivaPieGrande.onClick.AddListener(delegate
            {
                ZoomIn("SalivaPieGrande");
            });


            btnAceptarLiquidos.onClick.AddListener(GuardarProporcion);
            btnAceptarSolidos.onClick.AddListener(GuardarProporcion);
            btnBorrarSolidos.onClick.AddListener(BorrarProporcion);
            btnTeclado0.onClick.AddListener(delegate
            {
                SumarProporcion(0);
            });
            btnTeclado1.onClick.AddListener(delegate
            {
                SumarProporcion(1);
            });
            btnTeclado2.onClick.AddListener(delegate
            {
                SumarProporcion(2);
            });
            btnTeclado3.onClick.AddListener(delegate
            {
                SumarProporcion(3);
            });
            btnTeclado4.onClick.AddListener(delegate
            {
                SumarProporcion(4);
            });
            btnTeclado5.onClick.AddListener(delegate
            {
                SumarProporcion(5);
            });
            btnTeclado6.onClick.AddListener(delegate
            {
                SumarProporcion(6);
            });
            btnTeclado7.onClick.AddListener(delegate
            {
                SumarProporcion(7);
            });
            btnTeclado8.onClick.AddListener(delegate
            {
                SumarProporcion(8);
            });
            btnTeclado9.onClick.AddListener(delegate
            {
                SumarProporcion(9);
            });

            #endregion
        }

        // Update is called once per frame
        protected override void Update()
        {
            #region Initial state

            if (Instance.activeInHierarchy)
            {
                if (!esRetoActivo)
                {
                    GenericUi.ShowInfoPopUp(
                        "¡La escuela de magia Kadabra, se alegra de tenerte entre sus estudiantes! Deberás esforzarte porque el profesor Galaguer es uno de los más exigentes",
                        new ESequence
                        {
                            Action = (Challenge.Action)Action.ShowProfessor,
                            ChallengeNum = 1,
                            Event = Event.ClosePopUp
                        }, 0.0f
                    );
                    esRetoActivo = true;
                }
                else
                {
                    if (!string.IsNullOrEmpty(ingredienteActual) && !esZoomInTerminado &&
                        animZoom.GetCurrentAnimatorStateInfo(0).IsName(ingredienteActual) &&
                        animZoom.GetCurrentAnimatorStateInfo(0).normalizedTime % animZoom.GetCurrentAnimatorStateInfo(0).length > 1.0f)
                        ZoomOut(ingredienteActual);
                }
            }
            else
                esRetoActivo = false;

            #endregion

            AnimateText();
        }

        #endregion

        #region Private

        private void ClosePopUp(Action action)
        {
            switch (action)
            {
                #region Cases

                case Action.ShowEnemy:
                    goEnemigo.SetActive(true);
                    break;
                case Action.ShowProfessor:
                    goProfesor.SetActive(true);
                    animPuerta.Play("Puerta");
                    animPuerta.speed = 1f;
                    Invoke("MostrarGloboProfesor", 0.45f);
                    break;

                    #endregion
            }

            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
        }

        #endregion

        #endregion

        #region Methods

        #region Private

        private void BorrarProporcion()
        {
            proporcionSolido = string.Empty;
            txtProporcionSolido.text = "0";
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
        }

        private void ContabilizarIngredienteServido()
        {
            goIngredientes.transform.Find("btn" + ingredienteActual).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = false;
            goIngredientes.transform.Find("btn" + ingredienteActual).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = false;
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);

            if (++numIngredientesServidos == loRecetas[numPocionActual - 1].Mix.Count)
                goMezcla.SetActive(true);
        }

        private void GuardarProporcion()
        {
            if (loRecetas[numPocionActual - 1].Mix[ingredienteActual].Type == EIngredient.TypeIngredient.Liquid)
            {
                #region Guardar proporción de ingrediente líquido

                if ((int)sldMedidorLiquidos.value > 0)
                {
                    loRecetas[numPocionActual - 1].Mix[ingredienteActual].Proportion = Mathf.RoundToInt(sldMedidorLiquidos.value);
                    sldMedidorLiquidos.value = 0;
                    ContabilizarIngredienteServido();
                }

                goMedidorLiquidos.SetActive(false);

                #endregion
            }
            else
            {
                #region Guardar proporción de ingrediente sólido

                if (!string.IsNullOrEmpty(proporcionSolido))
                {
                    int proporcion = int.Parse(proporcionSolido);

                    if (proporcion != 0)
                    {
                        loRecetas[numPocionActual - 1].Mix[ingredienteActual].Proportion = proporcion;
                        ContabilizarIngredienteServido();
                    }

                    proporcionSolido = string.Empty;
                    txtProporcionSolido.text = "0";
                }

                goMedidorSolidos.SetActive(false);

                #endregion
            }
        }

        private void HabilitarZonaPreparacion()
        {
            goZonaBloqueo.SetActive(false);
            GenericUi.Resume();
        }

        private void InicializarPociones()
        {
            //ReSharper disable once UseObjectOrCollectionInitializer
            loRecetas = new List<ERecipe>();

            #region Initializing potion mix

            loRecetas.Add(new ERecipe
            {
                Dosage = 6,
                Mix = new Dictionary<string, EIngredient>
                {
                    {" Corteza", new EIngredient {Proportion = 0, Quantity = 200, Type = EIngredient.TypeIngredient.Solid}},
                    {" AguaCactus", new EIngredient {Color = new Color32(0x99, 0xE5, 0xA3, 0xFF), Proportion = 0, Quantity = 10, Type = EIngredient.TypeIngredient.Liquid}},
                    {" PlumaDeAvestruz", new EIngredient {Proportion = 0, Quantity = 2, Type = EIngredient.TypeIngredient.Solid}},
                    {" SalivaCaracol", new EIngredient {Color = new Color32(0x99, 0xE5, 0xA3, 0xFF), Proportion = 0, Quantity = 5, Type = EIngredient.TypeIngredient.Liquid}},
                    {"Aureola", new EIngredient {Proportion = 0, Quantity = 115, Type = EIngredient.TypeIngredient.Solid}}
                }

            });
            loRecetas.Add(new ERecipe
            {
                Dosage = 4,
                Mix = new Dictionary<string, EIngredient>
                {
                    {"CenizasDeFenix", new EIngredient {Proportion = 0, Quantity = 150, Type = EIngredient.TypeIngredient.Solid}},
                    {"RocaLunar", new EIngredient {Proportion = 0, Quantity = 47, Type = EIngredient.TypeIngredient.Solid}},
                    {" Rocio", new EIngredient {Color = new Color32 (0x04, 0xF0, 0xFF, 0xFF), Proportion = 0, Quantity = 20, Type = EIngredient.TypeIngredient.Liquid}},
                    {"SalivaDeOgro", new EIngredient {Color = new Color32 (0x00, 0xED, 0x00, 0xFF), Proportion = 0, Quantity = 5, Type = EIngredient.TypeIngredient.Liquid}}
                }

            });
            loRecetas.Add(new ERecipe
            {
                Dosage = 5,
                Mix = new Dictionary<string, EIngredient>
                {
                    {"UnaDeYeti", new EIngredient {Color = new Color32 (0x16, 0xD2, 0xD9, 0xFF), Proportion = 0, Quantity = 35, Type = EIngredient.TypeIngredient.Liquid}},
                    {"RioQueCanta", new EIngredient {Color = new Color32 (0x16, 0xD2, 0xD9, 0xFF), Proportion = 0, Quantity = 4, Type = EIngredient.TypeIngredient.Liquid}},
{"PoloNorte", new EIngredient {Color = new Color32 (0x16, 0xD2, 0xD9, 0xFF), Proportion = 0, Quantity = 15, Type = EIngredient.TypeIngredient.Liquid}},
                    {"RocaLunar", new EIngredient {Proportion = 0, Quantity = 24, Type = EIngredient.TypeIngredient.Solid}},
                    }
            });
            loRecetas.Add(new ERecipe
            {
                Dosage = 3,
                Mix = new Dictionary<string, EIngredient>
                {
                    {"HadaNegra", new EIngredient {Proportion = 0, Quantity = 80, Type = EIngredient.TypeIngredient.Solid}},
                    {"CapaDeMerilin", new EIngredient {Proportion = 0, Quantity = 55, Type = EIngredient.TypeIngredient.Solid}},
                    {"EsenciaDeClavel", new EIngredient {Color = new Color32 (0x4A, 0x5A, 0x5B, 0xFF), Proportion = 0, Quantity = 25, Type = EIngredient.TypeIngredient.Liquid}},
                    {"CuernoUnicornio", new EIngredient {Proportion = 0, Quantity = 280, Type = EIngredient.TypeIngredient.Solid}},
                    {"SalivaPieGrande", new EIngredient {Color = new Color32 (0x4A, 0x5A, 0x5B, 0xFF), Proportion = 0, Quantity = 30, Type = EIngredient.TypeIngredient.Liquid}},
                }

            });

            #endregion
        }

        private void MezclarIngredientes()
        {
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);

            goMezcla.SetActive(false);
            goReceta.SetActive(false);
            goPociones.SetActive(true);

            goPociones.transform.Find("btnPocion" + numPocionActual).GetComponent<Button>().enabled = false;

            ingredienteActual = string.Empty;
            numIngredientesServidos = 0;
            numPocionActual = 0;
        }

        [UsedImplicitly]
        private void MostrarGloboProfesor()
        {
            goGloboProfesor.SetActive(true);
            Texto = goGloboProfesor.transform.Find("txtGloboProfesor").GetComponent<Text>();
            SetTextToAnimate("Toca la puerta para iniciar el curso de Brebajes y pociones.", 4.0f);
        }

        private void MostrarLaboratorio()
        {
            goLaboratorio.SetActive(true);
            goInicio.SetActive(false);

            GenericUi.ShowInfoPopUp(
                "Prueba 1: vencer al Minotauro.\nSelecciona y prepara las pociones, indica los gramos o mililitros que corresponden a la dosis necesaria para que funcionen las pociones.",
                new ESequence
                {
                    Action = (Challenge.Action)Action.ShowEnemy,
                    ChallengeNum = 1,
                    Event = Event.ClosePopUp
                }, 0.0f
            );
        }

        private void MostrarReceta(int numPocion)
        {
            numPocionActual = numPocion;
            goPociones.SetActive(false);
            goReceta.SetActive(true);

            switch (numPocion)
            {
                #region Recetas

                case 1:
                    txtReceta.text =
                        "<size=24><b>APTERA:</b></size> Evita volar.\n\n" +
                        "Para <b>una</b> dosis:\n" +
                        "	200 gr de corteza de árbol llorón \n" +
                        "	10 ml de agua de cactus\n" +
                        "	2 gr de plumas de avestruz \n" +
                        "	5 ml de saliva de caracol\n" +
                        "	115 gr de aureola de ángel caído\n\n" +
                        "<b>PARA QUE FUNCIONE EN EL DRAGÓN NECESITAS: 6 DOSIS.</b>";
                    break;
                case 2:
                    txtReceta.text =
                        "<size=24><b>AVERTE:</b></size> : Apaga las llamas que puede lanzar.\n\n" +
                        "Para <b>una</b> dosis:\n" +
                    "	150 gr de ceniza del fénix\n" +
                    "	47 gr de roca lunar\n" +
                        "	20 ml de rocío\n" +
                        "	5 ml de saliva de un ogro\n\n" +
                        "<b>PARA QUE FUNCIONE EN EL DRAGÓN NECESITAS: 4 DOSIS.</b>";
                    break;
                case 3:
                    txtReceta.text =
                        "<size=24><b> DURATUS:</b></size> Enfría y evita que se mueva.\n\n" +
                        "Para <b>una</b> dosis:\n" +
                        "	35 gr de uña molida del Yeti\n" +
                    "	4 ml de agua de río que canta\n" +
                        "	15 ml de agua helada del polo norte\n" +
                        "	24 gr de roca lunar \n" +
                        "<b>PARA QUE FUNCIONE EN EL DRAGÓN NECESITAS: 5 DOSIS.</b>";
                    break;
                case 4:
                    txtReceta.text =
                        "<size=24><b> DEPELLENDAM:</b></size> Desvanece al enemigo cuando se encuentra débil.\n" +
                        "Para <b>una</b> dosis:\n" +
                        "	<size=20>80 gr de polvo de Hada Negra\n" +
                        "	55 gr de tela de la capa de Merlín\n" +
                        "	25 ml de esencia de clavel\n" +
                        "	280 gr de cuerno de Unicornio molido</size>\n" +
"	30 ml de saliva de Pie Grande </size>\n" +

                        "<b>PARA QUE FUNCIONE EN EL DRAGÓN NECESITAS: 3 DOSIS.</b>";
                    break;

                    #endregion
            }

            foreach (string ingrediente in loRecetas[numPocionActual - 1].Mix.Keys)
            {
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = true;
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = true;
            }

            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
        }

        private void SumarProporcion(int cantidad)
        {
            if (((cantidad == 0 && txtProporcionSolido.text != "0") || cantidad != 0) &&
                txtProporcionSolido.text.Length < 8)
            {
                proporcionSolido += cantidad;
                txtProporcionSolido.text = proporcionSolido;
                GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
            }
        }

        private void ZoomIn(string ingrediente)
        {
            if (numPocionActual > 0 && loRecetas[numPocionActual - 1].Mix.ContainsKey(ingrediente))
            {
                esZoomInTerminado = false;
                ingredienteActual = ingrediente;
                goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = true;
                animZoom = goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>();
                animZoom.Play(ingrediente, -1, 0f);
                animZoom.speed = 1f;
                GenericUi.SoundManager.PlaySound("audio-brebajes-y-pociones/zoom", 1);
            }
        }

        private void ZoomOut(string ingrediente)
        {
            esZoomInTerminado = true;

            if (loRecetas[numPocionActual - 1].Mix[ingrediente].Type == EIngredient.TypeIngredient.Liquid)
            {
                imgAreaInteractiva.color = loRecetas[numPocionActual - 1].Mix[ingrediente].Color;
                goMedidorLiquidos.SetActive(true);
            }
            else
                goMedidorSolidos.SetActive(true);
        }

        #endregion

        #region Protected

        /// <summary>
        /// Execute a serie of action/event of the sequence, after an specified time
        /// </summary>
        /// <param name="sequence">The action/event to execute</param>
        /// <param name="waitingTime">Waiting time to launch the action/event of the sequence</param>
        /// <returns></returns>
        internal override IEnumerator Execute(ESequence sequence, float waitingTime)
        {
            yield return new WaitForSeconds(waitingTime);

            switch (sequence.Event)
            {
                case Event.ClosePopUp:
                    ClosePopUp((Action)sequence.Action);
                    break;
            }
        }

        /// <summary>
        /// Set the challange to its initial state
        /// </summary>
        protected override void ReStart()
        {
            #region Reset objects to initial state

            foreach (string ingrediente in lsIngredientes)
            {
                goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = false;
                goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>().speed = 0f;
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = false;
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = false;
            }

            goPociones.transform.Find("btnPocion1r3").GetComponent<Button>().enabled = true;
            goPociones.transform.Find("btnPocion2r3").GetComponent<Button>().enabled = true;
            goPociones.transform.Find("btnPocion3r3").GetComponent<Button>().enabled = true;
            goPociones.transform.Find("btnPocion4r3").GetComponent<Button>().enabled = true;

            txtProporcionSolido.text = "0";
            txtReceta.text = string.Empty;

            goMedidorLiquidos.SetActive(false);
            goMedidorSolidos.SetActive(false);
            goZonaBloqueo.SetActive(true);
            goMezcla.SetActive(false);
            goReceta.SetActive(false);
            goPociones.SetActive(true);
            goEnemigo.SetActive(false);
            goLaboratorio.SetActive(false);
            goGloboProfesor.SetActive(false);
            goProfesor.SetActive(false);
            goInicio.SetActive(true);

            animPuerta.speed = 0f;

            esRetoActivo = false;
            esZoomInTerminado = false;
            sldMedidorLiquidos.value = 0;
            numIngredientesServidos = 0;
            numPocionActual = 0;
            ingredienteActual = string.Empty;
            proporcionSolido = string.Empty;
            InicializarPociones();

            #endregion
        }

        /// <summary>
        /// Set the state of the sequence
        /// </summary>
        /// <param name="state">The new state of the sequence</param>
        internal override void SetState(State state)
        {
            switch (state)
            {
                case State.Initial:
                    ReStart();
                    break;
                case State.Playing:
                    break;
                case State.Finished:
                    break;
            }
        }

        #endregion

        #endregion
    }
}


