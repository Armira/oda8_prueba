﻿using System.Collections.Generic;

namespace Assets.Scripts
{
	public class ERecipe
	{
		#region Properties

		public int Dosage { get; set; }
		public Dictionary<string, EIngredient> Mix { get; set; }

		#endregion
	}
}
